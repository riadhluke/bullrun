//
//  ViewController.swift
//  Bull Run
//
//  Created by Riadh Luke Bello on 7/23/21.
//

import UIKit
import AVFoundation
import CoreMotion
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet private weak var playerView: PlayerView?
    
    private let player = AVQueuePlayer()
    private let playerItem = AVPlayerItem(url: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4")!)
//    private var playerItem = AVPlayerItem(url: Bundle.main.url(forResource: "WeAreGoingOnBullrun", withExtension: "mp4")!)
    private var looper: AVPlayerLooper?
    
    private let motionManager = CMMotionManager()
    private let locationManager = CLLocationManager()
    private var lastLocation: CLLocation?
    
    private var appStateObservers: [AnyObject] = []
    private var errorObserver: AnyObject?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.looper = AVPlayerLooper(player: player, templateItem: playerItem)
        playerView?.player =  player
        
        locationManager.delegate = self
        startLocationUpdates()
        
        let notifCenter = NotificationCenter.default
        let didBecomeActive = notifCenter.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: .main) { [unowned self] _ in
            print("active")
            self.player.play()
            if !self.motionManager.isGyroActive {
                self.startGyroUpdates()
            }
        }
        let didEnterBackground = notifCenter.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: .main) { [unowned self] _ in
            print("background")
            self.player.pause()
            self.stopGyroUpdates()
        }
        appStateObservers.append(contentsOf: [didBecomeActive, didEnterBackground])
        
        errorObserver = player.observe(\.error, options: [.new]) { [unowned self] _, change in
            if let newValue = change.newValue,
               let error = newValue {
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        // add tap recognizer for some controls
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap(_:)))
        doubleTap.numberOfTapsRequired = 2
        view.addGestureRecognizer(singleTap)
        view.addGestureRecognizer(doubleTap)
        singleTap.require(toFail: doubleTap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        player.play()
        startGyroUpdates()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player.pause()
        stopGyroUpdates()
    }
    
    deinit {
        appStateObservers.forEach({ NotificationCenter.default.removeObserver($0) })
        errorObserver?.invalidate()
    }
    
    // MARK: - Tap Handlers
    @objc private func handleTap(_ sender: UITapGestureRecognizer) {
        if player.timeControlStatus == .paused {
            player.play()
        } else if player.timeControlStatus == .playing {
            player.pause()
        }
    }
    
    @objc private func handleDoubleTap(_ sender: UITapGestureRecognizer) {
        guard let playerLayer = playerView?.playerLayer else { return }
        if playerLayer.videoGravity == .resizeAspect {
            playerLayer.videoGravity = .resizeAspectFill
        } else if playerLayer.videoGravity == .resizeAspectFill {
            playerLayer.videoGravity = .resizeAspect
        }
    }
    
    // MARK: - Shake observer
    override func motionEnded(_ motion: UIEvent.EventSubtype,
                     with event: UIEvent?) {
        guard motion == .motionShake else { return }
        if player.timeControlStatus == .paused {
            player.play()
        } else if player.timeControlStatus == .playing {
            player.pause()
        }
    }
    
    // MARK: - Location
    private func startLocationUpdates() {
        let authStatus = locationManager.authorizationStatus
        switch authStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:
            break
        }
    }
    
    
    // MARK: - Gyroscope
    private func startGyroUpdates() {
        guard motionManager.isGyroAvailable else { return }
        motionManager.gyroUpdateInterval = 1/20
        motionManager.startGyroUpdates(to: .main) { data, error in
            let player = self.player
            if let data = data {
                // update volume from x rotation
                if abs(data.rotationRate.x) > 1 {
                    let newVolume = player.volume - Float(data.rotationRate.x) * 0.025
                    player.volume = min(1, max(0, newVolume))
                }
                
                // update current time from z rotation
                guard let playerItem = self.player.currentItem,
                      playerItem.duration.isNumeric else { return }
                let duration = playerItem.duration.seconds
                let skipLength = Double(1)
                if abs(data.rotationRate.z) > 1 {
                    let change = data.rotationRate.z/abs(data.rotationRate.z) * skipLength
                    let current = self.player.currentTime().seconds
                    var newTime = current - change
                    // loop around
                    if newTime < 0 {
                        newTime += duration
                    } else if newTime > duration {
                        newTime -= duration
                    }
                    if self.player.timeControlStatus != .waitingToPlayAtSpecifiedRate {
                        self.player.seek(to: CMTime(seconds: newTime, preferredTimescale: 100))
                    }
                }
            } else if let error = error {
                print(error)
            }
        }
    }
    
    private func stopGyroUpdates() {
        motionManager.stopGyroUpdates()
    }
}

// MARK: - CLLocationManagerDelegate -
extension ViewController: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        guard manager.authorizationStatus == .authorizedWhenInUse
                || manager.authorizationStatus == .authorizedAlways else { return }
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let newLocation = locations.last else { return }
        if let lastLocation = lastLocation {
            // prone to false positives, device location is not accurate enough to check change of 10 meters
            if lastLocation.distance(from: newLocation) > 10 {
                self.lastLocation = newLocation
                self.player.seek(to: CMTime(seconds: 0, preferredTimescale: 100))
            }
        } else {
            lastLocation = newLocation
        }
    }
    
}
